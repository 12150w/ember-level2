'use strict';

module.exports = function(environment, appConfig) {
	return {
		modulePrefix: 'addon',
		podModulePrefix: 'addon/pods',
		environment: environment,
		baseURL: '/',
		locationType: 'auto'
	};
};
