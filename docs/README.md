Ember Level 2 Addon Documentation
========================================================================================================================
This documentation will take you through the ember-level2 addon.


Components
------------------------------------------------------------------------------------------------------------------------

| Name                                                  | Description                                                  |
|-------------------------------------------------------|--------------------------------------------------------------|
| [l2-login](docs/components/l2-login.md)               | Login form component                                         |
| [l2-logout](docs/components/l2-logout.md)             | Button/Link that when clicked logs the user out              |

## Routes

| Name                                                   | URL                            |
|--------------------------------------------------------|--------------------------------|
| [manage-users](docs/routes/manage-users.md)            | `/manage/users`                |
| [manage-users/groups](docs/routes/manage-users/groups) | `/manage/user-groups`          |
| [manage-groups](docs/routes/manage-groups.md)          | `/manage/groups`               |

## Controllers

| Name                                                          | Description                                               |
|---------------------------------------------------------------|-----------------------------------------------------------|
| [manage-users](docs/controllers/manage-users.md)              | Allows creating and modifying users                       |
| [manage-users/groups](docs/controllers/manage-users/groups.md)| Allows adding/removing users in groups                    |
| [manage-groups](docs/controllers/manage-groups.md)            | Allows creating & modifying groups                        |
