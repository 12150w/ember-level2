l2-logout
===========================================================================================
A component that provides a link to log out a user


Options
-------------------------------------------------------------------------------------------

#### [redirect] `String`
Route name to redirect the user after logout (Default: `null`)


Example Usage
-------------------------------------------------------------------------------------------

### Simple Usage

```html
{{l2-logout}}
```

### Advanced Usage

```html
{{l2-login redirect="login"}}