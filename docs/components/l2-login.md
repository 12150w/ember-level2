l2-login
===========================================================================================
A component that provides a form to input user credentials log the user in


Options
-------------------------------------------------------------------------------------------

#### [usernameLabel] `String`
Text displayed next to the username input (Default: `'Username'`)

#### [passwordLabel] `String`
Text display next to the password input (Default: `'Password'`)

#### [inputClass] `String`
Sets class on the username and password inputs (Default: `null`)

#### [redirect] `String`
Route name to redirect the user after login (Default: `null`)


Example Usage
-------------------------------------------------------------------------------------------

### Simple Usage

```html
{{l2-login}}
```

### Advanced Usage

```html
{{l2-login
	usernameLabel="User Name"
	passwordLabel="Password"
	inputClass="form-control"
	redirect="home"
}}