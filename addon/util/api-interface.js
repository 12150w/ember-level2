import Ember from 'Ember';
import config from '../../config/environment';

export default Ember.Object.extend({
	params: null,
	
	request: function(options) {
		
		return new Ember.RSVP.Promise(function(resolve, reject) {
			
			// Check if options is a POJO
			if(Ember.typeOf(options) !== 'object') {
				return reject('No options hash');
			}
			
			// Check the options
			
			// Check 'url'
			if(Ember.isEmpty(options.url)) {
				return reject('url not specified');
			} else if(Ember.typeOf(options.url) !== 'string') {
				return reject('url must be a string');
			}
			
			// Check 'method'
			if(Ember.isEmpty(options.method)) {
				return reject('method not specified');
			} else if(Ember.typeOf(options.method) !== 'string') {
				return reject('method must be a string');
			}
			
			// Capitalize method; Check if method is GET, POST, PATCH or DELETE
			options.method = options.method.toUpperCase();
			if(!['GET', 'POST', 'PATCH', 'DELETE'].contains(options.method)) {
				return reject('method must be \'GET\', \'POST\', \'PATCH\' or \'DELETE\'');
			}
			
			// Check 'contentType'
			if(Ember.isEmpty(options.contentType)) {
				return reject('contentType must be specified');
			}
			
			// Set the base URL
			options.url = config.baseUrl + '/' + options.url;
			
			// Stringify data if content-type is JSON and a POJO was passed in
			if(
				Ember.typeOf(options.contentType) === 'application/json' &&
				Ember.typeOf(options.data) === 'object'
			) {
				options.data = JSON.stringify(options.data);
			}
			
			// Send AJAX request
			Ember.$.ajax(options).then(function(result) {
				if(result.code < 400) {
					resolve(result);
				} else {
					reject('Error (' + result.code + '): ' + result.error);
				}
			}).fail(function(reason) {
				reject('An unexpected error occured: ' + JSON.stringify(reason));
			});
		});
	},
	
	getParameter: function(name) {
		// Check if name is a valid string
		if(Ember.isEmpty(name) || Ember.typeOf(name) !== 'string') {
			return null;
		}
		
		// Parse the key value pairs out of the URL
		var pairs = window.location.search.substr('?').split('&');
		if(Ember.isEmpty(pairs)) {
			return null;
		}
		
		// If params is cached already, get the param value
		if(Ember.typeOf(this.get('params')) === 'class') {
			return this.get('params.' + name);
		}
		
		// Save each key value pair to a cache
		var params = Ember.Object.create({});
		Ember.$.each(pairs, function(pair) {
			var pieces = pair.split('&');
			
			// if there isn't a key value pair, skip this iteration
			if(pieces.length !== 2) {
				return;
			}
			
			// Store key value pair
			params.set(pieces[0], decodeURIComponent(pieces[1]));
		});
		
		// Cache parameters
		this.set('params', params);
		
		// Get the param value
		return this.get('params.' + name);
	}
});