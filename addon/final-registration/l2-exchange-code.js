import Ember from 'ember';

export default Ember.Component.extend({
	tagName: 'form',
	classNames: ['ink-form'],
	
	password: null,
	retype: null,
	
	msg: null,
	msgClass: null,
	
	showForm: true,
	showMessage: false,
	isRegistering: false,
	
	didInsertElement: function() {
		var code = this.getParam('code');
		
		// Check for a valid code in the URL
		if(Ember.typeOf(code) !== 'string' || Ember.isEmpty(code)) {
			this.set('msg', 'Invalid code in URL. Please retry the link in the email.');
			this.set('showMessage', true);
			this.set('showForm', false);
			return;
		} else {
			this.set('code', code);
		}
	},
	
	register: function() {
		var self = this;
		
		self.set('showMessage', false);
		self.set('isRegistering', true);
		
		// Attempt to register using the auth API
		self.request({
			url: 'auth/register/final',
			method: 'POST',
			contentType: 'application/JSON',
			data: {
				key: self.get('code'),
				password: self.get('password')
			}
		}).then(function() {
			self.set('msgClass', 'alert-success');
			self.set('msg', 'Registration complete!');
		}).fail(function(reason) {
			self.set('msgClass', 'alert-error');
			self.set('msg', 'Registration failed. ' + reason);
		}).finally(function() {
			self.set('showMessage', true);
			self.set('isRegistering', false);
		});
	}
});