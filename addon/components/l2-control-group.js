import Ember from 'ember';

export default Ember.Component.extend({
	tagName: 'div',
	classNames: ['control-group'],
	classNameBindings: [
		'inline:column-group', 'inline:gutters',
		'validation:validation', 'validation'
	],
	
	inline: false,
	validation: null,
	type: 'text',
	name: null,
	label: null,
	placeholder: '',
	value: null,
	tip: '',
	
	showLabel: function() {
		return !Ember.isEmpty(this.get('label'));
	}.property('label'),
	
	showCheckbox: function() {
		return this.get('type') === 'checkbox' || this.get('type') === 'radio';
	}.property('type'),
	
	showTip: function() {
		return !Ember.isEmpty(this.get('tip'));
	}.property('tip')
});