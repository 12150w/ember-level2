import Ember from 'ember';

export default Ember.Component.extend({
	// HTML properties
	tagName: 'div',
	classNames: ['ink-form'],
	
	// Option defaults
	usernameLabel: 'Username',
	passwordLabel: 'Password',
	loginButtonLabel: 'Login',
	inputClass: null,
	redirect: null,
	
	// Form values
	username: null,
	password: null,
	msgClass: null,
	msg: null,
	
	// Display booleans
	isAuthenticating: false,
	showMessage: false,
	
	actions: {
		login: function() {
			var self = this;
			
			self.set('isAuthenticating', true);
			self.set('showMessage', false);
			
			// Check the form
			if(
				Ember.isEmpty(self.get('username')) ||
				Ember.isEmpty(self.get('password'))
			) {
				self.set('msgClass', 'login-error');
				self.set('msg', 'Username and password are required');
				self.set('isAuthenticating', false);
				return;
			}
			
			// Send API request
			self.request({
				url: 'auth/login',
				method: 'POST',
				contentType: 'application/JSON',
				data: {
					username: self.get('username'),
					password: self.get('password')
				}
			}).then(function(result) {
				self.set('msgClass', 'alert-success');
				self.set('msg', 'Success' + JSON.stringify(result));
				
				// Redirect the user
				if(!!self.get('redirect')) {
					self.transitionToRoute(self.get('redirect'));
				}
			}).fail(function(reason) {
				self.set('msgClass', 'alert-error');
				self.set('msg', reason);
				self.set('showMessage', true);
			}).always(function() {
				self.set('isAuthenticating', false);
			});
		}
	}
});