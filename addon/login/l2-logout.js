import Ember from 'ember';

export default Ember.Component.extend({
	// HTML properties
	tagName: 'div',
	classNames: [],
	
	// Option defaults
	redirect: null,
	
	actions: {
		logout: function() {
			var self = this;
			
			// Send API request
			self.request({
				url: 'auth/logout',
				method: 'POST',
				contentType: 'application/JSON',
				data: {}
			}).then(function() {
				// Redirect the user
				if(!!self.get('redirect')) {
					self.transitionToRoute(self.get('redirect'));
				}
			});
		}
	}
});	