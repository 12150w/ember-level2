Ember Level2 Addon
=====================================================================================================================
The ember-level2 addon provides easy integration between the Level2 API and Ember.
Included in this addon is a set of components to make integration with the Level2 API simple
	as well as an administrative interface injected into the application.


Installation
---------------------------------------------------------------------------------------------------------------------

* `git clone` this repository
* `npm install`
* `bower install`


[Documentation](docs/README.md)
---------------------------------------------------------------------------------------------------------------------
Full documentation on the API and usage is available here.
