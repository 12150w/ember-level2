/* jshint node: true */
'use strict';

var level2 = require('level2-api');

module.exports = {
	name: 'ember-level2',
	
	serverMiddleware: function(config) {
		/* Express Middlewares
		 * 
		 * Starts the Level2 API
		 */
		var l2Options = config.options.level2 || {},
			l2APIPath = config.options.baseURL + 'l2-api',
			api = new level2.API(l2Options.apiName || 'Level2 API');
		
		// Set up the data source and adapter
		var apiAdapter = new level2.DataAdapter(),
			apiDataSource = new level2.DataSource(apiAdapter);
		api.setSource(apiDataSource);
		
		// Set up the auth provider
		api.setAuthProvider(new level2.AuthProvider());
		
		// Inject the API
		console.log('Running Level2 API at ' + l2APIPath);
		api.run(config.app, l2APIPath);
		
		config.app.get('/aha', function(req, res) {
			res.send('aha');
		});
	}
};
